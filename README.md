Travel API Client 
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the assignment (after starting the application) go to:

[http://localhost:9000/travel/index.html](http://localhost:9000/travel/index.html)



Changes:
-------
These are the changes i've done to the project given the time.

**frontend**
 - created the frontend location: simple-travel-ui
 - When served the searchbar component searches for matching airports via the simplet-travel-api
 - Every config is stored in environment/environment
 
**backend**
 - Added a cors policy so that the frontend is able to send it's request. (This should change to only development environment instead of all)
 - Added a security policy which exposes /travel/actuator/prometheus for dashboarding. 
 - Added Requesttiminginterceptor and requestcounterinterceptor for the prometheus dashboard
 - Added webmvcConfig to add the interceptors
 - OauthClientconfig sync and async requests are possible. 
 
 **dashboard**
 
 Instead of building my own dashboard I've chosen to use two opensource packages (Prometheus/grafana) which makes it possible to create monitoring stats from scraping web-endpoint
 I created a very simple docker compose file which starts both dashboards
 
 Run: `docker-compose up`
 to start prometheus and grafana.
 
 The dashboards still need to be configured and created because it will run on another system. But it gives a general idea where i wanted to go with this ;)
 
 **Creating Module**
 
 I've splitted the two projects and created 2 seperate gradle files. 
 The API relies on the build of the frontend which builds a jar. 
 I was unable given the time (and experience) to get the jar exposed with the build. 