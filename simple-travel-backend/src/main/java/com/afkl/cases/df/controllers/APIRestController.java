package com.afkl.cases.df.controllers;

import com.afkl.cases.df.Service.OauthConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RunAs;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@RestController
public class APIRestController {

    @Value("${API.endpoints.fares}")
    String fareUrl;

    @Value("${API.endpoints.airports}")
    String airportUrl;

    @Autowired
    @Qualifier("clientRestTemplate")
    private OAuth2RestOperations restTemplate;

    @Autowired
    OauthConnectionService oauthConnectionService;


    @RequestMapping(value= "/fares/{from}/{to}", method = GET)
    String getFares(@PathVariable("from") String from, @PathVariable("to") String to) {
        return oauthConnectionService.getResults(fareUrl + "/" + from + "/" + to, String.class, restTemplate);

    }
    @RequestMapping(value= "/fares/total/{from}/{to}", method = GET)
    String getTotal(@PathVariable("from") String from, @PathVariable("to") String to) throws Exception{
        List<Future<String>> futures = new ArrayList<Future<String>>();
        futures.add(oauthConnectionService.getAsynchronousResults(airportUrl + "?term="+from, String.class, restTemplate));
        futures.add(oauthConnectionService.getAsynchronousResults(airportUrl + "?term="+to, String.class, restTemplate));
        futures.add(oauthConnectionService.getAsynchronousResults(fareUrl + "/" + from + "/" + to, String.class, restTemplate));

        int total = futures.size();
        String response = "[";
        int received = 0;
        boolean errors = false;
        while (received < total && !errors){
            for (Future<String> future : futures) {
                try {
                    String result = future.get();
                    response += result + ',';
                    received++;
                } catch (Exception ex) {
                    errors = true;
                }
            }
        }
        return response.substring(0, response.length() - 1) + ']';
    }

    @RequestMapping(value= "/airport/{term}", method = GET)
    String getAirport(@PathVariable("term") String term){

        return oauthConnectionService.getResults(airportUrl + "?term="+term, String.class, restTemplate);
    }
}
