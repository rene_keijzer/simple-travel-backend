import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AirportService } from 'src/app/services/airport.service';
import {FormControl, ControlContainer} from '@angular/forms';
import { Airport } from 'src/models/Airport';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  airports: Airport[];
  airport: Airport;

  @Input()
  get chosenAirport(){
    return this.airport;
  }
  set chosenAirport(val){
    this.airport =  this.chosenAirport;
  }
  control = new FormControl();
  constructor(private airportService: AirportService) { 

  }

  @Input()
  textLabel:string; 

  @Output()
  airportChange: EventEmitter<Airport> = new EventEmitter();


    ngOnInit() {
     this.control.valueChanges.subscribe((value)=> {
       if(value && this.airports){
          this.airportChange.emit( this.airports.find((item) => item.name === value));       
       }
      this.findAirports(value).subscribe((airports)=>{
        this.airports = airports;
      });
    });
    
    
  }
  

  findAirports(value:string){
    return this.airportService.searchAirport(value);
  }

}
