import { Injectable } from '@angular/core';
import { FareAdapter, Fare } from 'src/models/Fare';
import { APIService } from './api.service';
import { environment} from '../../environments/environment';
import { Airport } from 'src/models/Airport';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FareService {
  url:string;
  name:string
  constructor(private apiService: APIService, private fareAdapter:FareAdapter) { 
    this.name='fares';
    this.url = `${this.apiService.baseUrl}/${environment.ApiConfig.restEndpoints[this.name]}`;
  }


  getFare(origin:Airport, destination:Airport):Observable<Fare>{
    return this.apiService.request(`${this.url}/${origin.code}/${destination.code}`).pipe(
      map((data:any) => this.fareAdapter.adapt(data))
    );
  }
}
