import { Injectable } from '@angular/core';
import { APIService } from './api.service';
import { environment} from '../../environments/environment';
import { AirportAdapter, Airport } from 'src/models/Airport';
import { map } from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AirportService{
  url:string;
  name:string;
  constructor(private apiService: APIService, private airportAdapter:AirportAdapter) { 
    this.name='airports';
    this.url = `${this.apiService.baseUrl}/${environment.ApiConfig.restEndpoints[this.name]}`;
  }

  getAllAirports(query?: string):Observable<Airport[]> {
    return this.apiService.request(`${this.url}/${query}`).pipe(
    map((data:any) => 
      data._embedded.locations.map(item => this.airportAdapter.adapt(item))
    ));
  }



  searchAirport(term: string, lang?:string) : Observable<Airport[]>{
    return lang ? this.getAllAirports(`?term=${term}&lang=${lang}`) : this.getAllAirports(`?term=${term}`);
  }

}


