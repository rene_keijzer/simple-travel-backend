import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OauthToken, OauthTokenAdapter } from 'src/models/OauthToken';
import { map } from 'rxjs/operators';
import {Observable, EMPTY} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class APIService {
  token: OauthToken;
  baseUrl = environment.ApiConfig.baseUrl;
  constructor(private httpClient: HttpClient, private oauthAdapter: OauthTokenAdapter) {
    this.updateOauthToken();
  }


  //updates oauth token if needed.
  async updateOauthToken(){
      this.token = await this.authorize().toPromise();
      console.log(this.token);
      
  }
  authorize(): Observable<OauthToken>{
        
    const encodedCredentials = btoa(`${environment.ApiConfig.client_id}:${environment.ApiConfig.secret}`);
   
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", `Basic ${encodedCredentials}`);
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    
    return this.httpClient.post(environment.ApiConfig.authorizationUrl, `grant_type=${environment.ApiConfig.grant_type}` ,{headers}).pipe(
      map((data: any) => this.oauthAdapter.adapt(data))
    );
  }

  //Just limiting to get requests because of time.
  request(url:string):Observable<any>{
    if(!this.token || !this.token.isValid()){
      console.log(!this.token);
     // console.log(!this.token.isValid());
      
      this.updateOauthToken();
      return EMPTY;
    }
      let headers = new HttpHeaders();
      headers = headers.append('Authorization', this.token.getAuthHeader());
      return this.httpClient.get(url, {headers});
      
    }
  }

