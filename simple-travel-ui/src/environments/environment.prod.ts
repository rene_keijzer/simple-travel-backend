export const environment = {
  production: true,
  ApiConfig: {
    baseUrl: "http://localhost:8080",
    authorizationUrl: "http://localhost:8080/oauth/token",
    grant_type:"client_credentials",
    client_id:'travel-api-client',
    secret:'psw',
    restEndpoints: {
      airports: '/airports',
      fares:'/fares'
    }
  }
};
