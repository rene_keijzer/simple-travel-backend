import { Injectable } from '@angular/core';
import {Adapter} from '../helpers/adapter';

export class Airport{
    code: string;
    name: string;
    description: string;
    coordinates: Coordinates;
    parent: Airport;

    constructor(code: string, name: string, description:string, coordinates: Coordinates, parent:Airport){
        this.code = code;
        this.name = name; 
        this.description = description;
        this.coordinates = coordinates;
        this.parent = parent;
    }
}


export class Coordinates{
    latidude: number;
    longitude: number;
}

@Injectable({
    providedIn:'root'
})
export class AirportAdapter implements Adapter<Airport>{
    adapt(item:any): Airport{
        return new Airport(item.code, item.name, item.description, item.coordinates as Coordinates,  item.parent? this.adapt(item.parent): undefined);
    }
}