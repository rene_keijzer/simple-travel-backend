import { Adapter } from 'src/helpers/adapter';
import {Injectable} from '@angular/core';

export class Fare{

    amount: number;
    currency: "EUR"| "USD";
    origin: string;
    destination: string;
    constructor(amount: number, currency: "EUR"|"USD", origin:string, destination:string){
        this.amount = amount;
        this.currency = currency;
        this.origin = origin;
        this.destination = destination;
    }
}
@Injectable({
    providedIn:'root'
})
export class FareAdapter implements Adapter<Fare>{
    adapt(item: any): Fare{
        return new Fare(item.amount, item.currency, item.origin, item.destination);
    }
}

